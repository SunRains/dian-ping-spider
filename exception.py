# _*_coding:utf-8_*_
# Author:sy
# Created:2021/11/6 0006 10:28
# Version:1.0
# Description: 异常处理类

class SpiderException(Exception):
    pass


class UtilsException(Exception):
    pass


class ProxyException(Exception):
    pass


class ExcelException(Exception):
    pass

class RequestException(Exception):
    pass
